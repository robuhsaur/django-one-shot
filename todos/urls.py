from django.urls import path

from todos.views import (
    create_item,
    create_todo,
    delete_todo,
    edit_todo,
    show_list,
    show_detail,
    update_item,
)

urlpatterns = [
    path("", show_list, name="todo_list_list"),
    path("<int:pk>/", show_detail, name="todo_list_detail"),
    path("create/", create_todo, name="todo_list_create"),
    path("<int:pk>/edit/", edit_todo, name="todo_list_update"),
    path("<int:pk>/delete/", delete_todo, name="todo_list_delete"),
    path("items/create/", create_item, name="todo_item_create"),
    path("items/<int:pk>/edit/", update_item, name="todo_item_update"),
]
