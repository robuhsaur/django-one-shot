from django.shortcuts import get_object_or_404, redirect, render
from todos.forms import TodoListForm, TodoItemForm
from django.views.generic.edit import CreateView

from todos.models import TodoItem, TodoList

# Create your views here.
def show_list(request):
    todo_list = TodoList.objects.all()
    context = {"todolist": todo_list}

    return render(request, "todos/list.html", context)


def show_detail(request, pk):
    context = {"todolist": TodoList.objects.get(pk=pk)}

    return render(request, "todos/detail.html", context)


def create_todo(request):
    context = {}
    form = TodoListForm(request.POST or None)
    if form.is_valid():
        todo = form.save()
        return redirect("todo_list_detail", pk=todo.pk)
    context = {"form": form}
    return render(request, "todos/create.html", context)


def edit_todo(request, pk):
    context = {}
    obj = get_object_or_404(TodoList, pk=pk)
    form = TodoListForm(request.POST or None, instance=obj)
    if form.is_valid():
        todo = form.save()
        return redirect("todo_list_detail", pk=todo.pk)
    context["form"] = form
    return render(request, "todos/edit.html", context)


def delete_todo(request, pk):
    context = {}
    obj = get_object_or_404(TodoList, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html", context)


def create_item(request):
    context = {}
    form = TodoItemForm(request.POST or None)
    if form.is_valid():
        todo = form.save()
        return redirect("todo_list_detail", pk=todo.list.pk)
    context = {"form": form}
    return render(request, "todo_items/create_item.html", context)


def update_item(request, pk):
    context = {}
    todo_item = get_object_or_404(TodoItem, pk=pk)
    form = TodoItemForm(request.POST or None, instance=todo_item)
    if form.is_valid():
        todo = form.save()
        return redirect("todo_list_detail", pk=todo.list.pk)
    context["form"] = form

    return render(request, "todo_items/update_item.html", context)
