# Generated by Django 4.0.6 on 2022-07-27 21:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todos', '0007_alter_todolist_created_on'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todoitem',
            name='due_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
