* [x] Fork and clone the starter project from django-one-shot 
* [x] Create a new virtual environment in the repository directory for the project
* [x] Activate the virtual environment
* [x] Upgrade pip
* [x] Install django
* [x] Install black
* [x] Install flake8
* [x] Install djlint
* [x] Deactivate your virtual environment
* [x] Activate your virtual environment
* [x] Use pip freeze to generate a requirements.txt file
* [x] git add .
        git commit -m "Feature 1 Complete"
        git push origin main
    

LE FEATURE TWO 
* [x] Create a Django project named brain_two so that the manage.py file is in the top directory
  * [x] django-admin startproject «name» .
    
* [x] Create a Django app named todos and install it in the brain_two Django project in the INSTALLED_APPS list
  * [x] "app_name.apps.AppNameConfig"
* [x] Run the migrations
* [x] Create a super user


LE FEATURE THREE
* [x] Make a class 
* [x] add attributes to the class
* [x] add a function if they ask for one 
  

LE FEATURE FOUR
* [x] Start server
* [x] go to the admin page
* [x] add todo list 
* [x] git add/commit/push

LE FEATURE FIVE 
* [x] Create TodoItem model in the todos app
* [x] class TodoItem 
  * [x] task, due_date, is_completed, list
* [x] add __str__ function
* [x] make migrations and migrate
* [x] Test 
* [x] git add/commit/push 


LE FEATURE SIX 
* [x] Register the TodoItem model in admin
* [x] git add/commit/push 
  
LE FEATURE SEVEN
* [x] Create a view that will get all of the instances of the TodoList model and put them in the context for the template.
* [x] Register that view in the todos app for the path "" and the name "todo_list_list" in a new file named todos/urls.py.
* [x] Include the URL patterns from the todos app in the brain_two project with the prefix "todos/".
* [x] Create a template for the list view that complies with the following specifications.
* [x] resulting html should go to /todos
* [x] HTML should have fundamental 5 
  * [x] a main tag that contains:
        div tag that contains:
            an h1 tag with the content "My Lists"
                a table that has two columns:
                    -the first with the header "Name" and the rows with the names of the Todo lists
                    -the second with the header "Number of items" and nothing in those rows because we don't yet have tasks




LE FEATURE ELEVEN
* [x] Create a delete view for the TodoList model that will show a delete button and handle the form submission to delete an existing TodoList.
* [x] If the to-do list is successfully deleted, it should redirect to the to-do list list view.
* [x] Register that view for the path /todos/<int:pk>/delete in the todos urls.py and the name "todo_list_delete".
* [x] Create an HTML template that shows the form to delete a new TodoList (see the template specifications below).
* [ ] Add a link to the detail view for the TodoList that navigates to the new delete view.



LE FEATURE TWELVE 
* [x] Create a create view for the TodoItem model that will show the task field in the form and handle the form submission to create a new TodoItem.
* [x] If the to-do list is successfully created, it should redirect to the detail page for that to-do list.
* [x] Register that view for the path "items/create/" in the todos urls.py and the name "todo_item_create".
* [x] Create an HTML template that shows the form to create a new TodoItem (see the template specifications below).
* [x] Add a link to the list view for the TodoList that navigates to the new create view.    


LE FEATURE THIRTEEN
* [ ] Create an update view for the TodoItem model that will show the task field, the due date field, an is_completed checkbox, and a select list showing the to-do lists in the form and handle the form submission to change an existing TodoItem.
* [ ] If the to-do item is successfully edited, it should redirect to the detail page for the to-do list.
* [ ] Register that view for the path /todos/items/<int:pk>/edit in the todos urls.py and the name "todo_item_update".
* [ ] Create an HTML template that shows the form to edit a TodoItem (see the template specifications below).
* [ ] Add a link to the list view for the TodoList that navigates to the new update view.
